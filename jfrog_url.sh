


jfrogCredential="readonly:Welcome%4001@igtb.jfrog.io";

digital_gatekeeper_url="https://${jfrogCredential}/igtb/libs-release-local/com/igtb/digital/gatekeeper/digital-gatekeeper/${digital_gatekeeper_ver}/digital-gatekeeper-${digital_gatekeeper_ver}.jar";

digital_gatekeeper_external_url="https://${jfrogCredential}/igtb/libs-release-local/com/igtb/digital/gatekeeper/digital-gatekeeper/${digital_gatekeeper_ver}/digital-gatekeeper-${digital_gatekeeper_ver}.jar";

digital_conductor_url="https://${jfrogCredential}/igtb/gradle-release-local/com/igtb/digital/conductor-server/${digital_conductor_ver}/conductor-server-${digital_conductor_ver}-all.jar";

digital_approvalwf_url="https://${jfrogCredential}/igtb/libs-release-local/com/igtb/digital/api/digital-approvalwf-standalone/${digital_approvalwf_ver}/digital-approvalwf-standalone-${digital_approvalwf_ver}.jar";


digital_limits_url="https://${jfrogCredential}/igtb/libs-release-local/com/igtb/digital/api/action-limits-standalone/${digital_limits_ver}/action-limits-standalone-${digital_limits_ver}.jar";

digital_audit_url="https://${jfrogCredential}/igtb/libs-release-local/com/igtb/digital/digital-audit/${digital_audit_ver}/digital-audit-${digital_audit_ver}.jar";

digital_chronos_url="https://${jfrogCredential}/igtb/libs-release-local/com/igtb/digital/digital-chronos/${digital_chronos_ver}/digital-chronos-${digital_chronos_ver}.jar";

digital_config_url="https://${jfrogCredential}/igtb/libs-release-local/com/igtb/cfg/digital-config/${digital_config_ver}/digital-config-${digital_config_ver}.jar";

digital_registry_url="https://${jfrogCredential}/igtb/libs-release-local/com/igtb/registry/digital-registry/${digital_registry_ver}/digital-registry-${digital_registry_ver}.jar";

digital_alerts_url="https://${jfrogCredential}/igtb/libs-release-local/com/igtb/api/alert-engine/${digital_alerts_ver}/alert-engine-${digital_alerts_ver}.jar";

digital_alertpref_url="https://${jfrogCredential}/igtb/libs-release-local/com/igtb/digital/api/action-alertpreferences-standalone/${digital_alertpref_ver}/action-alertpreferences-standalone-${digital_alertpref_ver}.jar"


digital_userpref_url="https://${jfrogCredential}/igtb/libs-release-local/com/igtb/digital/api/action-userpreferences-standalone/${digital_userpref_ver}/action-userpreferences-standalone-${digital_userpref_ver}.jar"


digital_impersonation_manager_url="https://${jfrogCredential}/igtb/libs-release-local/com/igtb/digital/digital-impersonation-manager-standalone/${digital_impersonation_manager_ver}/digital-impersonation-manager-standalone-${digital_impersonation_manager_ver}.jar";

action_sweeps_url="https://${jfrogCredential}/igtb/libs-release-local/com/igtb/digital/api/action-sweeps-standalone/${action_sweeps_ver}/action-sweeps-standalone-${action_sweeps_ver}.jar"

action_user_impersonation_url="https://${jfrogCredential}/igtb/libs-release-local/com/igtb/digital/api/action-user-impersonation-standalone/${action_user_impersonation_ver}/action-user-impersonation-standalone-${action_user_impersonation_ver}.jar";

ingestion_common_services_url="https://${jfrogCredential}/igtb/libs-release/com/igtb/digital/ingestion/ingestion-common-services/${ingestion_common_services_ver}/ingestion-common-services-${ingestion_common_services_ver}.jar"

ingestion_customers_url="https://${jfrogCredential}/igtb/libs-release/com/igtb/digital/customer-ingestion-k4-ftb/${ingestion_customers_ver}/customer-ingestion-k4-ftb-${ingestion_customers_ver}.jar";

ingestion_accounts_url="https://${jfrogCredential}/igtb/libs-release/com/igtb/digital/ingestion/accounts-ingestion-k4-ftb/${ingestion_accounts_ver}/accounts-ingestion-k4-ftb-${ingestion_accounts_ver}.jar"


ingestion_users_url="https://${jfrogCredential}/igtb/libs-release-local/com/igtb/api/ingestion/ingestion-users/${ingestion_users_ver}/ingestion-users-${ingestion_users_ver}.jar"

ingestion_accessprofile_url="https://${jfrogCredential}/igtb/libs-release-local/com/igtb/digital/ingestion/ingestion-accessprofile/${ingestion_accessprofile_ver}/ingestion-accessprofile-${ingestion_accessprofile_ver}.jar"


ingestion_approvalwf_url="https://${jfrogCredential}/igtb/libs-release-local/com/igtb/digital/ingestion/ingestion-approvalwf/${ingestion_approvalwf_ver}/ingestion-approvalwf-${ingestion_approvalwf_ver}.jar"

ingestion_limits_url="https://${jfrogCredential}/igtb/libs-release-local/com/igtb/digital/api/ingestion-limits/${ingestion_limits_ver}/ingestion-limits-${ingestion_limits_ver}.jar";

ingestion_liquidity_sweeps_url="https://${jfrogCredential}/igtb/libs-release-local/com/igtb/digital/ingestion-liquidity-sweeps-standalone/${ingestion_liquidity_sweeps_ver}/ingestion-liquidity-sweeps-standalone-${ingestion_liquidity_sweeps_ver}.jar"

ingestion_liquidity_commons_url="https://${jfrogCredential}/igtb/libs-release-local/com/igtb/digital/ingestion-liquidity-commons-standalone/${ingestion_liquidity_commons_ver}/ingestion-liquidity-commons-standalone-${ingestion_liquidity_commons_ver}.jar"

ingestion_roles_url="https://${jfrogCredential}/igtb/libs-release-local/com/igtb/api/ingestion/ingestion-roles/${ingestion_roles_ver}/ingestion-roles-${ingestion_roles_ver}.jar"

state_ingestion_limits_url="https://${jfrogCredential}/igtb/libs-release-local/com/igtb/stateingestion/state-ingestion-limits/${state_ingestion_limits_ver}/state-ingestion-limits-${state_ingestion_limits_ver}.jar"

action_user_impersonation_url="https://${jfrogCredential}/igtb/libs-release-local/com/igtb/digital/api/action-user-impersonation-standalone/${action_user_impersonation_ver}/action-user-impersonation-standalone-${action_user_impersonation_ver}.jar"




