#!/bin/sh
#set -x 
set -eo pipefail

# Slurp in Vault static secrets, this overrides 
# properties set via configMaps or secrets.
if [ -f /etc/secrets/app-credentials ]; then
   source /etc/secrets/app-credentials
fi

# Spring Cloud Config client needs to pass the Vault access token
if [ -f /etc/secrets/vault-token ]; then
   export SPRING_CLOUD_CONFIG_TOKEN=$(cat /etc/secrets/vault-token)
fi

# Digital Registry service bindings
if [ ! -z "$REGISTRY_PROTOCOL" -a ! -z "$REGISTRY_HOSTNAME" -a ! -z "$REGISTRY_PORT" ]; then
   export EUREKA_CLIENT_SERVICEURL_DEFAULTZONE=${REGISTRY_PROTOCOL}://$REGISTRY_USERNAME:$REGISTRY_PASSWORD@${REGISTRY_HOSTNAME}:${REGISTRY_PORT}/eureka/
else
   echo "Error - incomplete configuration for Digital-Registry service provided, exit."
   exit 1
fi


# Read Certificate detail and create certificate file and add to java keystore
IFS=","

for CERT_COMPONENT in ${CERTIFICATES_COMPONENTES[@]}
do
     CERTIFICATE_VAR="${CERT_COMPONENT}"_CERT
     CERTIFICATE=${!CERTIFICATE_VAR}

     echo $CERTIFICATE > ${CERT_COMPONENT}.crt

    #import certificate to trust store
     keytool -noprompt -import -trustcacerts -keystore "$JAVA_HOME/jre/lib/security/cacerts" \
      -storepass changeit \
      -alias $CERT_COMPONENT  -import \
      -file "${CERT_COMPONENT}.crt"
done

unset IFS


java $JAVA_OPTS $APP_OPTS -Djava.security.egd=file:/dev/./urandom -jar /cbx-service.jar

