#java
digital_gatekeeper_ver=0.0.8
digital_gatekeeper_external_ver=$digital_gatekeeper_ver
digital_gatekeeper_ver=0.0.8
digital_conductor_ver=0.0.34
digital_approvalwf_ver=0.3.2
digital_chronos_ver=1.0.6
digital_config_ver=1.0.2
digital_registry_ver=1.0.2
digital_alerts_ver=0.2.1
digital_userpref_ver=0.0.14
digital_impersonation_manager_ver=1.1.5
action_sweeps_ver=1.2.1
ingestion_common_services_ver=0.0.5
ingestion_customers_ver=0.0.7
ingestion_accounts_ver=0.0.25
ingestion_users_ver=1.2.3
 #1.1.9
ingestion_accessprofile_ver=1.0.6
ingestion_approvalwf_ver=1.0.8
ingestion_liquidity_sweeps_ver=2.0.17
ingestion_liquidity_commons_ver=2.0.9
ingestion_roles_ver=1.0.3
action_user_impersonation_ver=1.0.6
#Node
ingestion_account_balances_ver=0.3.18
digital_gazetteer_ver=1.3.7
digital_quest_ver=0.5.35
notification_dispatcher_ver=0.1.66
#"0.4.0"
#Static
cbx_web_ver=0.6.0-rc.5
#cbx_web_ver=0.1.0-beta.29
#"0.4.1-rc.19"
digital_conductor_ui_ver=0.0.18



