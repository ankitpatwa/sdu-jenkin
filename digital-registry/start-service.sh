#!/bin/sh
#set -x 
set -eo pipefail

# Slurp in Vault static secrets, this overrides 
# properties set via configMaps or secrets.
if [ -f /etc/secrets/app-credentials ]; then
   source /etc/secrets/app-credentials
fi

java $JAVA_OPTS $APP_OPTS -Djava.security.egd=file:/dev/./urandom -jar /cbx-service.jar