#!/bin/sh
#set -x 
set -eo pipefail

# Slurp in Vault static secrets, this overrides 
# properties set via configMaps or secrets.
if [ -f /etc/secrets/app-credentials ]; then
   source /etc/secrets/app-credentials

   export SPRING_PROFILES_ACTIVE=git,vault
   export SPRING_CLOUD_CONFIG_SERVER_VAULT_HOST=$(echo $VAULT_ADDR | awk -F[/:] '{print $4}')
   export SPRING_CLOUD_CONFIG_SERVER_VAULT_PORT=$(echo $VAULT_ADDR | awk -F[/:] '{print $5}')
   export SPRING_CLOUD_CONFIG_SERVER_VAULT_SCHEME=$(echo $VAULT_ADDR | awk -F[/:] '{print $1}')
   export SPRING_CLOUD_CONFIG_SERVER_VAULT_BACKEND=$VAULT_SECRETS_PATH

   # If we use a Vault instance with self-signed certificate...
   if [ "$VAULT_SELF_SIGNED_CERT" = "true" -a -f /etc/certificates/vault.crt ]; then
    keytool -noprompt -import -trustcacerts -keystore "$JAVA_HOME/jre/lib/security/cacerts" \
      -storepass changeit \
      -alias vault -import \
      -file "/etc/certificates/vault.crt"
   fi

   # Optionally, there is support for the Vault Enterprise X-Vault-Namespace header. 
   # To have it sent to Vault set the namespace property.
   if [ ! -z "$VAULT_NAMESPACE" ]; then
      export SPRING_CLOUD_CONFIG_SERVER_VAULT_NAMESPACE=$VAULT_NAMESPACE
   fi
fi

# Digital Registry service bindings
if [ ! -z "$REGISTRY_PROTOCOL" -a ! -z "$REGISTRY_HOSTNAME" -a ! -z "$REGISTRY_PORT" ]; then
   export EUREKA_CLIENT_SERVICEURL_DEFAULTZONE=${REGISTRY_PROTOCOL}://$REGISTRY_USERNAME:$REGISTRY_PASSWORD@${REGISTRY_HOSTNAME}:${REGISTRY_PORT}/eureka/
else
   echo "Error - incomplete configuration for Digital-Registry service provided, exit."
   exit 1
fi

java $JAVA_OPTS $APP_OPTS -Djava.security.egd=file:/dev/./urandom -jar /cbx-service.jar